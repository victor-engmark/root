{ config, pkgs, ... }:
{
  environment = {
    etc = {
      "xdg/baloofilerc".source = (pkgs.formats.ini { }).generate "baloorc" {
        "Basic Settings" = {
          "Indexing-Enabled" = false;
        };
      };
      "xdg/user-dirs.defaults".text = builtins.readFile ../includes/xdg-user-dirs.defaults;
    };
    sessionVariables = {
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME = "$HOME/.local/share";
      XDG_STATE_HOME = "$HOME/.local/state";
    };
  };
  xdg.mime.defaultApplications = {
    "application/msword" = [
      "onlyoffice-desktopeditors.desktop"
      "writer.desktop"
    ];
    "application/pdf" = [
      "org.gnome.Evince.desktop"
      "com.github.xournalpp.xournalpp.desktop"
    ];
    "application/vnd.ms-excel" = [
      "onlyoffice-desktopeditors.desktop"
      "calc.desktop"
    ];
    "application/vnd.ms-powerpoint" = [
      "onlyoffice-desktopeditors.desktop"
      "impress.desktop"
    ];
    "application/vnd.oasis.opendocument.text" = "writer.desktop";
    "application/vnd.openxmlformats-officedocument.presentationml.presentation" = [
      "onlyoffice-desktopeditors.desktop"
      "impress.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.presentationml.slideshow" = [
      "onlyoffice-desktopeditors.desktop"
      "impress.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.presentationml.template" = [
      "onlyoffice-desktopeditors.desktop"
      "impress.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" = [
      "onlyoffice-desktopeditors.desktop"
      "calc.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.spreadsheetml.template" = [
      "onlyoffice-desktopeditors.desktop"
      "calc.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = [
      "onlyoffice-desktopeditors.desktop"
      "writer.desktop"
    ];
    "application/vnd.openxmlformats-officedocument.wordprocessingml.template" = [
      "onlyoffice-desktopeditors.desktop"
      "writer.desktop"
    ];
    "application/vnd.rar" = [
      "org.gnome.FileRoller.desktop"
      "mcomix.desktop"
    ];
    "application/x-matroska" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "application/x-xpinstall" = "org.gnome.FileRoller.desktop";
    "application/zip" = [
      "org.gnome.FileRoller.desktop"
      "mcomix.desktop"
    ];
    "audio/aac" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/mp4" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/mpeg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/mpegurl" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/ogg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/vnd.rn-realaudio" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/vorbis" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-flac" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-mp3" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-mpegurl" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-ms-wma" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-musepack" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-oggflac" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-pn-realaudio" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-scpls" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-vorbis" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-vorbis+ogg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "audio/x-wav" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "image/bmp" = [
      "feh.desktop"
      "gimp.desktop"
    ];
    "image/heif" = [
      "feh.desktop"
      "gimp.desktop"
    ];
    "image/jpeg" = [
      "feh.desktop"
      "gimp.desktop"
    ];
    "image/png" = [
      "feh.desktop"
      "gimp.desktop"
    ];
    "image/svg+xml" = "org.inkscape.Inkscape.desktop";
    "image/webp" = [
      "feh.desktop"
      "gimp.desktop"
    ];
    "text/html" = [
      config.home-manager.users.victor.programs.firefox.package.desktopItem.name
      "chromium-browser.desktop"
    ];
    "text/markdown" = "org.gnome.TextEditor.desktop";
    "text/plain" = "org.gnome.TextEditor.desktop";
    "text/x-log" = "org.gnome.TextEditor.desktop";
    "text/xml" = [
      config.home-manager.users.victor.programs.firefox.package.desktopItem.name
      "chromium-browser.desktop"
    ];
    "video/3gp" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/3gpp2" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/3gpp" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/avi" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/divx" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/dv" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/fli" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/flv" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/mp2t" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/mp4" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/mp4v-es" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/mpeg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/msvideo" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/ogg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/quicktime" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/vnd.divx" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/vnd.mpegurl" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/vnd.rn-realvideo" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/webm" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-avi" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-flv" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-m4v" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-matroska" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-mpeg2" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-ms-asf" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-msvideo" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-ms-wmv" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-ms-wmx" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-ogm" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-ogm+ogg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-theora" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "video/x-theora+ogg" = [
      "mpv.desktop"
      "vlc.desktop"
    ];
    "x-scheme-handler/http" = [
      config.home-manager.users.victor.programs.firefox.package.desktopItem.name
      "chromium-browser.desktop"
    ];
    "x-scheme-handler/https" = [
      config.home-manager.users.victor.programs.firefox.package.desktopItem.name
      "chromium-browser.desktop"
    ];
  };
}
