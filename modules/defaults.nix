{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}:
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  environment = {
    defaultPackages = lib.mkForce [ ];
    systemPackages = [
      pkgs.extundelete # Recover deleted files from an ext3 or ext4 partition
      pkgs.ntfs3g # FUSE-based NTFS driver with full write support
    ];
  };

  hardware = {
    bluetooth.enable = true;
    sane.enable = true;
  };

  i18n.supportedLocales = [ "all" ];

  networking = {
    networkmanager = {
      enable = true;
      ensureProfiles = {
        environmentFiles = [ config.sops.secrets.wifi.path ];
        profiles =
          {
            home = {
              connection = {
                id = "$HOME_SSID";
                type = "wifi";
              };
              wifi.ssid = "$HOME_SSID";
              wifi-security = {
                key-mgmt = "wpa-psk";
                psk = "$HOME_PASSWORD";
              };
            };
          }
          // (builtins.listToAttrs (
            map (
              id:
              lib.nameValuePair id {
                connection = {
                  autoconnect-priority = lib.toInt id; # Connect to later SSIDs first
                  id = "$SSID_${id}";
                  type = "wifi";
                };
                wifi.ssid = "$SSID_${id}";
                wifi-security = {
                  key-mgmt = "wpa-psk";
                  psk = "$PASSWORD_${id}";
                };
              }
            ) (map (number: builtins.toString number) (lib.range 1 27))
          ));
      };
    };
    resolvconf.dnsExtensionMechanism = false;
    stevenblack.enable = true;
    useDHCP = false;
  };

  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    settings = {
      experimental-features = [
        "flakes"
        "nix-command"
      ];
      download-buffer-size = 500000000; # 500 MB
      trusted-users = [
        config.users.users.root.name
        "@${config.users.groups.wheel.name}"
      ];
    };
  };

  programs.fuse.userAllowOther = true;

  services = {
    acpid.enable = true;
    avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = true;
    };
    fail2ban.enable = true;
    fwupd.enable = true;
    geoclue2.enableDemoAgent = lib.mkForce true;
    gvfs.enable = true;
    printing = {
      cups-pdf.enable = true;
      enable = true;
    };
    tzupdate.enable = true;
  };

  sops = {
    age = {
      keyFile = "${config.users.users.victor.home}/.config/sops/age/keys.txt";
      sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    };
    defaultSopsFile = ../secrets/default.yaml;
    secrets = {
      wifi = { };
    };
  };

  users.users.victor.extraGroups = [
    config.users.groups.lp.name # See https://nixos.wiki/wiki/Scanners#Installing_scanner_support
    config.users.groups.networkmanager.name # For managing network connections
    config.users.groups.scanner.name # For https://nixos.wiki/wiki/Scanners#Installing_scanner_support
    config.users.groups.wheel.name # For sudo
  ];
}
