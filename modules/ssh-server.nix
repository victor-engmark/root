{
  services.openssh = {
    enable = true;
    settings = {
      AllowGroups = [ "users" ];
      KbdInteractiveAuthentication = false;
      KexAlgorithms = [
        "curve25519-sha256"
        "curve25519-sha256@libssh.org"
        "sntrup761x25519-sha512@openssh.com"
      ];
      LoginGraceTime = 0;
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };
}
