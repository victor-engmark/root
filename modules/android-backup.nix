{ config, pkgs, ... }:
{
  systemd.services.android-backup = {
    enableStrictShellChecks = true;
    path = [
      pkgs.android-tools
      pkgs.git
      pkgs.parallel-full
    ];
    script = builtins.readFile ../includes/android-backup.bash;
    serviceConfig = {
      CapabilityBoundingSet = "";
      Group = config.users.groups.adbusers.name;
      IPAddressAllow = "127.0.0.1";
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      NoNewPrivileges = true;
      PrivateNetwork = true;
      PrivateTmp = true;
      PrivateUsers = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      ProtectSystem = true;
      RemoveIPC = true;
      RestrictAddressFamilies = "AF_INET AF_NETLINK";
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      RuntimeDirectory = config.systemd.services.android-backup.name;
      SystemCallArchitectures = "native";
      SystemCallFilter = "~@clock @cpu-emulation @debug @module @mount @obsolete @privileged @raw-io @reboot @swap";
      UMask = "0077";
      User = config.users.users.victor.name;
    };
    startAt = "daily";
  };
}
