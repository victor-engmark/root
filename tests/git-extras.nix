{ pkgs }:
let
  nixosLib = import (pkgs.path + "/nixos/lib") { };

  machineName = "machine";
in
nixosLib.runTest {
  hostPkgs = pkgs;

  name = "git-extras";

  nodes.${machineName} = {
    environment.systemPackages = [ (import ../overrides/git-extras { package = pkgs.git; }) ];
    users.users.anybody.isNormalUser = true;
  };

  testScript = ''
    ${machineName}.start()

    ${machineName}.succeed("git config --global alias.my-alias 'subcommand arguments'")
    ${machineName}.succeed("git aliases | grep --quiet '^my-alias subcommand arguments$'")
    ${machineName}.succeed("git help aliases | grep --quiet SYNOPSIS")
    ${machineName}.succeed("git aliases --help | grep --quiet SYNOPSIS")
    ${machineName}.succeed("man 1 git-aliases | grep --quiet SYNOPSIS")
  '';
}
