{
  system ? builtins.currentSystem,
  pkgs ? import <nixpkgs> { inherit system; },
}:
{
  gitWithExtras = import ./git-extras.nix { inherit pkgs; };
  fontconfigDtd = import ./fontconfig-dtd.nix { inherit pkgs; };
  sshClientAudit = import ./ssh-client-audit.nix { inherit pkgs; };
  sshServerAudit = import ./ssh-server-audit.nix { inherit pkgs; };
}
