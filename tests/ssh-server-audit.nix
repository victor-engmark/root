{ pkgs }:
let
  nixosLib = import (pkgs.path + "/nixos/lib") { };

  machineName = "machine";
in
nixosLib.runTest {
  hostPkgs = pkgs;

  name = "ssh-server-audit";

  nodes.${machineName} = {
    imports = [ ../modules/ssh-server.nix ];
    system.stateVersion = "24.11";
  };

  testScript = ''
    ${machineName}.start()

    # Wait for SSH daemon service to start
    ${machineName}.wait_for_unit("sshd.service")

    # Should pass SSH server audit
    ${machineName}.succeed("${pkgs.ssh-audit}/bin/ssh-audit 127.0.0.1")
  '';
}
