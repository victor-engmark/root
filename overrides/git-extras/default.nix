{ package }:
package.overrideAttrs (old: {
  postInstall =
    ''
      for script_path in ${./scripts}/*; do
          script_filename="$(basename "$script_path")"
          command_name="''${script_filename%.bash}"

          install --mode=755 "$script_path" "$out/bin/$command_name"
      done

      cp ${./Documentation}/*.txt Documentation/
    ''
    + old.postInstall;
})
