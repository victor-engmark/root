git-co-author(1)
================

NAME
----
git-co-author - Add co-author(s) of the most recent commit

SYNOPSIS
--------
[verse]
'git co-author' [<co-author>...]

DESCRIPTION
-----------
When run without any parameters, this adds a
"Co-Authored-By: Your Name <\you@example.org>" paragraph to the bottom of the
most recent commit message, using the name from the Git configuration
`user.name` and the email address from `user.email`. You can also run it with
specific `Full Name <email>` arguments to add each one to a separate
`Co-Authored-By` line in the last commit.

EXAMPLES
--------
`git co-author`::

	Add a "Co-Authored-By: [your name and email address]" paragraph to the
	bottom of the most recent commit message.

`git co-author 'Jane Doe <jdoe@example.com>' 'Joe Schmoe <schmoe@example.org>'`::

	Add
+
----
Co-Authored-By: Jane Doe <jdoe@example.com>
Co-Authored-By: Joe Schmoe <schmoe@example.org>
----
+
to the bottom of the most recent commit message.

SEE ALSO
--------
linkgit:git-commit[1]

BUGS
----
Please report bugs at <https://gitlab.com/engmark/root/-/issues>.
