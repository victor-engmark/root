default_branch="$(git default-branch)"
current_branch="$(git current-branch)"
merged_branches_string="$(git branch --merged="${default_branch}" --no-contains="${default_branch}" --no-contains="${current_branch}" --format='%(refname:short)')"
mapfile -t merged_branches <<< "${merged_branches_string}"
${merged_branches+git branch --delete "${merged_branches[@]}"}
