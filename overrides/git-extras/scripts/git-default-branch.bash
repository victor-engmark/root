default_remote="$(git default-remote)"
remote_branch_reference="$(git symbolic-ref --short "refs/remotes/${default_remote}/HEAD")"
basename "${remote_branch_reference}"
