set -o errexit -o nounset

upstream="$1"
downstream="$2"

default_downstream_remote="$(git default-remote)"

# Clone upstream and call the remote "upstream"
git clone --origin=upstream "${upstream}"

# Add default downstream remote
cd "$(basename "${upstream}" .git)"
git remote add "${default_downstream_remote}" "${downstream}"

# Point current branch to default downstream remote
current_branch="$(git current-branch)"
git config "branch.${current_branch}.remote" "${default_downstream_remote}"
