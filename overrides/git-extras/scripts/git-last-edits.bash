set -o pipefail

wrapper() { git ls-tree --name-only -r -z HEAD "$@" | TZ=UTC xargs -I_ --max-procs=0 --no-run-if-empty --null -- git --no-pager log --date=iso-local --max-count=1 --pretty=tformat:%ad%x09_ -- _; }
wrapper --
