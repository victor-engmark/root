set -o pipefail

git config --get-regexp '^alias\.' | cut --delimiter=. --fields 2-
