git stash save
rm .git/index
root="$(git rev-parse --show-toplevel)"
git checkout HEAD -- "${root}"
git stash pop
