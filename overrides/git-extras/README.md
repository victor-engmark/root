# Git extras

Additional subcommands and documentation for Git.

## Use

Pass any Git package to the override to add the extra subcommands to the
derivation.

Example:

<!-- editorconfig-checker-disable -->

```nix
{
  gitPackage = import ./overrides/git-extras {
    package = pkgs.gitFull;
  };
}
```

<!-- editorconfig-checker-enable -->

## Scripts

Put `git-foo.bash` in the `scripts` directory to create the `foo` subcommand.

## Documentation

See text files for examples. The format is whatever Git is using for
[their documentation](https://github.com/git/git/tree/master/Documentation). The
filenames must be the same as the corresponding script file, with a "txt"
extension instead of "bash". For example, `scripts/git-foo.bash` should be
accompanied by `Documentation/git-foo.txt`.
