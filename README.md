# root

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Automated NixOS configuration setup - see [flake.nix](flake.nix).

## Use

Add the path of module files to your own `/etc/nixos/configuration.nix` imports:

<!-- editorconfig-checker-disable -->

```nix
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    "/…/root/modules/defaults.nix"
    "/…/root/modules/dev.nix"
    # etc
  ];
}
```

<!-- editorconfig-checker-enable -->

### Secrets

Edit secrets file using `sops edit secrets/default.yaml`.

If using Vim, you can renumber the `SSID_N` + `PASSWORD_N` lines using
`let count=1 | g/\(SSID\|PASSWORD\)_/s/[0-9][0-9]\?/\=count/ | g/PASSWORD_/let count=count+1`.

## Bugs

Please submit bugs using the
[project issue tracker](https://gitlab.com/engmark/root/-/issues).

## Develop

Prerequisites:

- [Nix](https://nixos.org/download)
- [direnv](https://github.com/direnv/direnv)

To install Git hooks for verifying commits and contents:
`pre-commit install --hook-type=commit-msg --hook-type=pre-commit --overwrite`

See [.gitlab-ci.yml](.gitlab-ci.yml) for how to build, test, etc.

### Debugging

To set everything up and then go straight into an interactive Python shell
without running the tests, run
`"$(nix-build --attr driverInteractive tests/…)/bin/nixos-test-driver"`.

To go into the Python debugger during a test, add `breakpoint()` and run
`"$(nix-build --attr driver tests/…)/bin/nixos-test-driver"`

## License

[GPL v3 or later](LICENSE)

## Code of Conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

## Copyright

© 2014 Victor Engmark
